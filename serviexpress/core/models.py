from django.db import models
from usuario.models import Usuarioweb


class Mecanico(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100)
    telefono = models.IntegerField()
    email = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'mecanico'


class Reservadehora(models.Model):
    id = models.AutoField(primary_key=True)
    fecha_solicitud = models.DateField()
    asunto = models.CharField(max_length=200)
    tiposervicio = models.ForeignKey('Tiposervicio', on_delete=models.CASCADE)
    usuarioweb = models.ForeignKey(Usuarioweb, on_delete=models.CASCADE)

    class Meta:
        managed = True
        db_table = 'reservadehora'


class Servicio(models.Model):
    id = models.AutoField(primary_key=True)
    fecha = models.DateField()
    comentario = models.CharField(max_length=200, blank=True, null=True)
    validado = models.BooleanField()
    mecanico = models.ForeignKey(Mecanico, on_delete=models.CASCADE)
    reservadehora = models.ForeignKey(Reservadehora, on_delete=models.CASCADE)

    class Meta:
        managed = True
        db_table = 'servicio'


class Tiposervicio(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=100)
    precio = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'tiposervicio'