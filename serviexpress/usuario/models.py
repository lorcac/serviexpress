from django.db import models
from django.contrib.auth.models import AbstractUser

class Usuarioweb(AbstractUser):
    fecha_nacimiento = models.DateField(verbose_name='fecha de nacimiento')

    class Meta:
        db_table = 'usuarioweb'


